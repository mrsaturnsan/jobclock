/******************************************************************************/
/*
* @file   colormod.cpp
* @author Aditya Harsh
* @brief  Color modification.
*/
/******************************************************************************/

#include "colormod.h"

#include <iostream>

#if defined(_WIN32)
    #include <curses.h>
#elif defined(__APPLE__) || defined(__linux__) || defined(__unix__)
    #include <ncurses.h>
#endif

namespace JobClock
{
    namespace Color
    {
        /**
         * @brief Sets the color of the console bg/fg.
         * 
         * @param code 
         */
        void SetConsoleColor(Code code)
        {
            std::cout << "\033[" << static_cast<unsigned>(code) << "m";
            refresh();
        }
    }
}
