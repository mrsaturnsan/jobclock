#OS-Agnostic Makefile
OS := $(shell uname)

CFLAGS = -O3 -g -Wall -Wextra -pedantic

ifeq ($(OS),Linux)
CC = clang++-4.0
CFLAGS += -std=c++14
LINK = -lncurses
else
ifeq ($(OS),Darwin)
CC = clang++
CFLAGS += -std=c++1z
LINK = -lncurses
else
CC = g++
CFLAGS += -std=c++14
LINK = -lpdcurses
endif
endif

FILE = main.cpp colormod.cpp write2console.cpp jobclock.cpp
OUT = jobclock

build:
	@$(CC) $(CFLAGS) $(FILE) $(LINK) -o $(OUT)

run:
	@./$(OUT)

clean:
	@rm $(OUT)