/******************************************************************************/
/*
* @file   jobclock.cpp
* @author Aditya Harsh
* @brief  Job clocking program.
*/
/******************************************************************************/

#include "jobclock.h"

#if defined(_WIN32)
    #include "mingw.thread.h"
    #include <curses.h>
    #define NL '\r'
#elif defined(__APPLE__) || defined(__linux__) || defined(__unix__)
    #include <thread>
    #include <ncurses.h>
    #define NL '\n'
#endif

#include <fstream>

#if defined(__APPLE__) || defined(__linux__) || defined(__unix__)
    #include <signal.h> 
    #include <assert.h>
#endif

#include "helper.h"
#include "colormod.h"
#include "write2console.h"

namespace JobClock
{
    namespace
    {
    #if defined(__APPLE__) || defined(__linux__) || defined(__unix__)
        void SignalHandler(int sig) noexcept
        {
            std::exit(sig);
        }

        void InstallSignal() noexcept
        {
            // struct of signal handling data
            struct sigaction act;

            // set values
            sigemptyset(&act.sa_mask);
            act.sa_flags = 0;
            act.sa_handler = SignalHandler;

            // install signal handling
            assert(!(sigaction(SIGINT, &act, NULL) == -1) && 
                "Could not install signal handler");
            assert(!(sigaction(SIGQUIT, &act, NULL) == -1) && 
                "Could not install signal handler");
            assert(!(sigaction(SIGUSR1, &act, NULL) == -1) && 
                "Could not install signal handler");
            assert(!(sigaction(SIGUSR2, &act, NULL) == -1) && 
                "Could not install signal handler");
        }
    #endif
    }

    /**
     * @brief Constructor for the clock.
     * 
     * @param text_speed 
     * @param anim_length 
     * @param anim_speed 
     */
    Clock::Clock(unsigned text_speed, unsigned anim_length, unsigned anim_speed) : text_speed_(text_speed), grow_(true),
                                                                                   anim_length_(anim_length), anim_speed_(anim_speed), 
                                                                                   hours_(0), minutes_(0), seconds_(0), pause_time_(0), preserve_(false)
    {

    }

    /**
     * @brief Destructor.
     * 
     */
    Clock::~Clock()
    {
        // reset console color back to what it originally was
        Color::SetConsoleColor(Color::Code::BG_DEFAULT);
        Color::SetConsoleColor(Color::Code::FG_DEFAULT);
        endwin();

        // unexpected shutdown (save file just in case)
        if (!preserve_)
            SaveFileEmergency();
    }

    /**
     * @brief Initializes the system
     * 
     */
    void Clock::Init()
    {
        // install signals
    #if defined(__APPLE__) || defined(__linux__) || defined(__unix__)
        InstallSignal();
    #endif

        initscr();
        raw();
        cbreak();
        echo();
        keypad(stdscr, TRUE);
        scrollok(stdscr, TRUE);
        nodelay(stdscr, TRUE);

        // print intro
        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("\n+ Welcome to Aditya's Job Clock!\n\n", text_speed_);
        Writer::PrintTimed("+ Options:\n\n", text_speed_);
        Color::SetConsoleColor(Color::Code::FG_WHITE);
        Writer::PrintTimed("- Begin (b) - Start the timer\n", text_speed_);
        Writer::PrintTimed("- Delay (d) - Start the timer after a delay\n", text_speed_);
        Writer::PrintTimed("- Exit  (e) - Exit the program\n\n", text_speed_);
        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("+ Please enter a command: ", text_speed_);
    }

    /**
     * @brief Configures the system.
     * 
     * @return true 
     * @return false 
     */
    bool Clock::Config()
    {
        try
        {
            return TryConfig();
        }
        catch (const std::exception &e)
        {
            Writer::Print(e.what());
        }

        return false;
    }

    /**
     * @brief Checks if config went through.
     * 
     * @return true 
     * @return false 
     */
    bool Clock::TryConfig()
    {
        Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);

        do
        {
            // get input
            std::string input = Writer::GetString();

            // convert all letters to lowercase
            for (auto &&i : input)
                if (std::isalpha(i))
                    i = std::tolower(i);

            if (input == "begin" || input == "b")
            {
                // formatting
                Writer::Print("\n");
                break;
            }
            else if (input == "delay" || input == "d")
            {
                Delay();
                break;
            }
            else if (input == "exit" || input == "e")
            {
                return false;
            }
            else
            {
                // error
                Color::SetConsoleColor(Color::Code::FG_WHITE);
                Writer::PrintTimed("\n+ Invalid input!\n\n", text_speed_);
                Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
                Writer::PrintTimed("+ Please enter a command: ", text_speed_);
                Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
            }
        } while (1);

        // formatting
        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("+ Tracking: (press 'ENTER' to pause)\n\n", text_speed_);
        Color::SetConsoleColor(Color::Code::FG_WHITE);

        return true;
    }

    /**
     * @brief Delay timer.
     * 
     */
    void Clock::Delay()
    {
        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("\n+ Please specify a delay in seconds: ", text_speed_);
        Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);

        unsigned delay;
        while (1)
        {
            std::string input = Writer::GetString();
            std::string::size_type sz;
            int temp = std::stoi (input, &sz);
            if (temp < 0)
            {
                Color::SetConsoleColor(Color::Code::FG_WHITE);
                Writer::PrintTimed("\n+ Invalid input\n\n", text_speed_);
                Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
            }
            else
            {
                delay = static_cast<unsigned>(temp);
                break;
            }
        }
        
        Writer::Print("\n");
        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("+ Starting in: ", text_speed_);
        Color::SetConsoleColor(Color::Code::FG_WHITE);
        for (unsigned i = delay; i > 0; --i)
        {
            Writer::Print(std::to_string(i));
        #if defined(_WIN32)
            mingw_stdthread::this_thread::sleep_for(std::chrono::milliseconds(1));
        #else
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
        #endif
            Writer::Erase(snprintf(0, 0, "%+d", i) - 1);
        }
        Writer::Print("0\n\n");
        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
    }

    /**
     * @brief Runs the clock.
     * 
     */
    void Clock::Run()
    {
        // get the starting time
        time_start_ = std::chrono::steady_clock::now();

        while (1)
        {
            while (getch() != NL)
            {
                ShowCurrent();
                std::this_thread::sleep_for(std::chrono::milliseconds(anim_speed_));
                Writer::ClearLineMultiple(Writer::Direction::UP, 5);
            }

            if (ExitConfirmation())
                break;
        }

        // prompt the user to save their progress
        PromptSave();
    }

    /**
     * @brief Animate the timer.
     * 
     */
    void Clock::Animate()
    {
        if (grow_)
        {
            animation_ += '=';
            if (animation_.length() > anim_length_)
                grow_ = false;
        }
        else
        {
            animation_.erase(animation_.end() - 1);
            if (animation_.length() <= 1)
                grow_ = true;
        }
    }

    /**
     * @brief Show the current progress.
     * 
     */
    void Clock::ShowCurrent()
    {
        // get the time in seconds
        auto total_seconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - time_start_).count() - pause_time_;

        // calculate hours, minutes, and seconds
        hours_ = total_seconds / pow(60, 2);
        minutes_ = (total_seconds / 60) - (hours_ * 60);
        seconds_ = total_seconds - (60 * minutes_) - (pow(60, 2) * hours_);

        Writer::Print("- Hours:   ");
        Writer::Print(std::to_string(hours_));
        Writer::Print("\n");
        Writer::Print("- Minutes: ");
        Writer::Print(std::to_string(minutes_));
        Writer::Print("\n");
        Writer::Print("- Seconds: ");
        Writer::Print(std::to_string(seconds_));
        Writer::Print("\n\n");

        // fancy little animation
        Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
        Animate();
        Writer::Print(animation_);
        Color::SetConsoleColor(Color::Code::FG_WHITE);
    }

    /**
     * @brief Exit confirmation.
     * 
     * @return true 
     * @return false 
     */
    bool Clock::ExitConfirmation()
    {
        auto first = std::chrono::steady_clock::now();

        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("+ Are you done? ", text_speed_);

        Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
        Writer::PrintTimed("(y/n): ", text_speed_);

        nodelay(stdscr, FALSE);

        int response = getch();

        while (response != 'y' && response != 'n')
        {
            Color::SetConsoleColor(Color::Code::FG_WHITE);
            Writer::PrintTimed("\n\n+ Invalid input\n\n", text_speed_);
            Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
            Writer::PrintTimed("+ Are you done? ", text_speed_);
            Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
            Writer::PrintTimed("(y/n): ", text_speed_);
            response = getch();
        }

        nodelay(stdscr, TRUE);

        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);

        if (response == 'n')
        {
            Writer::ClearLine();
            Writer::PrintTimed("+ Tracking: (press 'ENTER' to pause)\n\n", text_speed_);
            pause_time_ += std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - first).count();
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * @brief Prompt for saving.
     * 
     */
    void Clock::PromptSave()
    {
        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("\n\n+ Save your progress? ", text_speed_);

        Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
        Writer::PrintTimed("(y/n): ", text_speed_);

        nodelay(stdscr, FALSE);

        int response = getch();

        while (response != 'y' && response != 'n')
        {
            Color::SetConsoleColor(Color::Code::FG_WHITE);
            Writer::PrintTimed("\n\n+ Invalid input\n\n", text_speed_);
            Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
            Writer::PrintTimed("+ Save your progress? ", text_speed_);
            Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
            Writer::PrintTimed("(y/n): ", text_speed_);
            response = getch();
        }

        // do a double confirmation
        if (response == 'n')
        {
            Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
            Writer::PrintTimed("\n\n+ Are you sure? ", text_speed_);

            Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
            Writer::PrintTimed("(y/n): ", text_speed_);

            response = getch();

            while (response != 'y' && response != 'n')
            {
                Color::SetConsoleColor(Color::Code::FG_WHITE);
                Writer::PrintTimed("\n\n+ Invalid input\n\n", text_speed_);
                Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
                Writer::PrintTimed("+ Are you sure? ", text_speed_);
                Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
                Writer::PrintTimed("(y/n): ", text_speed_);
                response = getch();
            }
            if (response == 'y')
            {
                nodelay(stdscr, TRUE);
                preserve_ = true;
                return;
            }
        }

        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("\n\n+ Pick a name for your file: ", text_speed_);

        std::time_t cur_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

        Color::SetConsoleColor(Color::Code::FG_LIGHT_RED);
        std::string file_name = Writer::GetString();

        // exit out if no name was provided
        if (file_name.empty() || file_name[0] == NL || file_name[0] == '\0')
        {
            Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
            nodelay(stdscr, TRUE);
            return;
        }

        // attach extension
        std::size_t found = file_name.find('.');
        if (found == std::string::npos)
            file_name += ".txt";

        std::ofstream file(file_name, std::ios_base::app);

        file << std::ctime(&cur_time)
        << "Hours worked:  " << hours_ << NL
        << "Minutes worked:" << minutes_ << NL
        << "Seconds worked:" << seconds_ << NL;

        Color::SetConsoleColor(Color::Code::FG_LIGHT_GREEN);
        Writer::PrintTimed("\n+ Progress saved.\n\n", text_speed_);

        nodelay(stdscr, TRUE);
    }

    void Clock::SaveFileEmergency()
    {
        std::time_t cur_time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        std::string file_name = "temp.txt";
        std::ofstream file(file_name, std::ios_base::app);
        file << std::ctime(&cur_time)
        << "Hours worked:  " << hours_ << NL
        << "Minutes worked:" << minutes_ << NL
        << "Seconds worked:" << seconds_ << NL;
    }
}
