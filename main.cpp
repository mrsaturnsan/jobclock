/******************************************************************************/
/*
* @file   main.cpp
* @author Aditya Harsh
* @brief  Program entry.
*/
/******************************************************************************/

#include "jobclock.h"
#include <memory>

/**
 * @brief Hack for getting destructors to be called
 * 
 */
namespace
{
    std::unique_ptr<JobClock::Clock> jclock;
}

/**
 * @brief Main function.
 * 
 * @return int 
 */
int main()
{
    jclock = std::make_unique<JobClock::Clock>(10, 30, 300);

    // check if memory was allocated
    if (!jclock.get())
        return -1;

    jclock->Init();

    if (!jclock->Config())
        return 0;

    jclock->Run();

    // clean execution
    return 0;
}