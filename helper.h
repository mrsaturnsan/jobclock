/******************************************************************************/
/*
* @file   helper.h
* @author Aditya Harsh
* @brief  Holds useful helper functions.
*/
/******************************************************************************/

namespace JobClock
{
    /**
     * @brief Power function.
     * 
     * @tparam T 
     * @param value 
     * @param times 
     * @return constexpr T 
     */
    template <typename T>
    constexpr T pow(const T& value, unsigned times)
    {
        if (times == 0)
            return T(1);
        return value * pow(value, times - 1);
    }
}
