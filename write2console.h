/******************************************************************************/
/*
* @file   write2console.h
* @author Aditya Harsh
* @brief  Writes output to the console.
*/
/******************************************************************************/

#pragma once

#include <string>

namespace JobClock
{
    /**
     * @brief Helper functionsfor writing to the console.
     * 
     */
    namespace Writer
    {
        enum class Direction
        {
            UP,
            DOWN,
            LEFT,
            RIGHT
        };

        void PrintTimed(const std::string& message, unsigned ms);
        void Print(const std::string& message);
        std::string GetString();
        void ClearLine();
        void ClearLineMultiple(Direction dir, unsigned times);
        void MoveCursor(Direction dir, unsigned times);
        void Erase(unsigned num);
    }
}