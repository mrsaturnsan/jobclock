/******************************************************************************/
/*
* @file   write2console.cpp
* @author Aditya Harsh
* @brief  Writes output to the console.
*/
/******************************************************************************/

#include "write2console.h"

#if defined (_WIN32)
    #include "mingw.thread.h"
    #include <curses.h>
#else
    #include <thread>
    #include <ncurses.h>
#endif

namespace JobClock
{
    namespace Writer
    {
        /**
         * @brief Prints out a string one by one.
         * 
         * @param message 
         * @param ms 
         */
        void PrintTimed(const std::string& message, unsigned ms)
        {
            for (auto&& i : message)
            {
                printw("%c", i);
                refresh();
            #ifdef _WIN32
                mingw_stdthread::this_thread::sleep_for(std::chrono::milliseconds(ms));
            #else
                std::this_thread::sleep_for(std::chrono::milliseconds(ms));
            #endif
            }
        }

        /**
         * @brief Prints a given message;
         * 
         * @param message 
         */
        void Print(const std::string& message)
        {
            printw("%s", message.c_str());
            refresh();
        }

        /**
         * @brief Gets a string.
         * 
         * @return std::string
         */
        std::string GetString()
        {
            char c_arr[64];

            nodelay(stdscr, FALSE);
            wgetnstr(stdscr, c_arr, 64);
            nodelay(stdscr, TRUE);
            
            return std::string(c_arr);
        }

        /**
         * @brief Clears the current line.
         * 
         */
        void ClearLine()
        {
            int y, x;
            getyx(stdscr, y, x); // save current pos
            move(y, 0); 
            clrtoeol();
        }

        /**
         * @brief Clears multiple lines.
         * 
         * @param dir 
         * @param times 
         */
        void ClearLineMultiple(Direction dir, unsigned times)
        {
            for (unsigned i = 0; i < times; ++i)
            {
                ClearLine();

                // don't move for last one
                if (i < (times - 1))
                    MoveCursor(dir, 1);
            }
        }

        /**
         * @brief Moves the cursor in any direction.
         * 
         * @param dir 
         */
        void MoveCursor(Direction dir, unsigned times)
        {
            // validity checking
            if (times == 0)
                throw std::runtime_error("Invalid number of times shifted!");

            int y, x;
            getyx(stdscr, y, x);

            switch(dir)
            {
            case Direction::UP:
                for (unsigned i = 0; i < times; ++i)
                    move(y - 1, x);
                break;
            case Direction::DOWN:
                for (unsigned i = 0; i < times; ++i)
                    move(y + 1, x);
                break;
            case Direction::LEFT:
                for (unsigned i = 0; i < times; ++i)
                    move(y, x - 1);
                break;
            case Direction::RIGHT:
                for (unsigned i = 0; i < times; ++i)
                    move(y, x + 1);
                break;
            default:
                throw std::runtime_error("Invalid enum");
            }
        }

        /**
         * @brief Specifies number of characters to erase.
         * 
         * @param num 
         */
        void Erase(unsigned num)
        {
            for (unsigned i = 0; i < num; ++i)
            {
                MoveCursor(Direction::LEFT, 1);
                delch();
                refresh();
            }
        }
    }
}