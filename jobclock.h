/******************************************************************************/
/*
* @file   jobclock.h
* @author Aditya Harsh
* @brief  Job clocking program.
*/
/******************************************************************************/

#pragma once

#include <string>
#include <chrono>

namespace JobClock
{
    class Clock
    {
    public:
        // class functions
        Clock(unsigned text_speed, unsigned anim_length, unsigned anim_speed);
        ~Clock();

        void Init();
        bool Config();
        void Run();
    
    private:
        // define time
    #if defined(__APPLE__)
            using time = std::__1::chrono::time_point<std::__1::chrono::steady_clock, std::__1::chrono::nanoseconds>;
    #elif defined(__linux__) || defined(__unix__)
            using time = std::chrono::time_point<std::chrono::_V2::steady_clock, std::chrono::nanoseconds>;
    #elif defined(_WIN32)
            using time = std::chrono::time_point<std::chrono::_V2::steady_clock, std::chrono::nanoseconds>;
    #endif

        // how fast text is rendered
        unsigned text_speed_;
        // current state of the animatin
        bool grow_;
        // length of the animation
        unsigned anim_length_;
        // speed of the animation
        unsigned anim_speed_;
        // data
        unsigned hours_;
        unsigned minutes_;
        unsigned seconds_;
        unsigned pause_time_;
        // the current frame of the animation
        std::string animation_;
        // the time
        time time_start_;
        // preserve
        bool preserve_;

        bool TryConfig();
        void ShowCurrent();
        bool ExitConfirmation();
        void Delay();
        void Animate();
        void PromptSave();
        void SaveFileEmergency();
    };
}